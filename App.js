/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  FlatList,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Alert

} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

/*
const DATA = [
  {
    Id: "40161234560023",
    ProductName: "VISA Signature Debit",
    Provider: "VISA",
    Virtual: false,
    Status: "Active",
    Accounts: [
  {
Number: "10000915377",
IsPrimary: true,
Balance: "5500"
}
]
  },
  {
    Id: "40161234560248",
ProductName: "VISA Signature Debit",
Provider: "VISA",
Virtual: false,
Status: "Inactive",
Accounts: [
{
Number: "10000915377",
IsPrimary: true,
Balance: "120500"
}
]
  },
  {
    Id: "40161234560987",
ProductName: "VISA Signature Debit",
Provider: "VISA",
Virtual: false,
Status: "Blocked",
Accounts: [
{
Number: "10000915377",
IsPrimary: true,
Balance: "1020500"
}
]
  },
  {
    Id: "40161234560230",
ProductName: "VISA Signature Debit",
Provider: "VISA",
Virtual: true,
Status: "Active",
Accounts: [
{
Number: "10000915377",
IsPrimary: true,
Balance: "5500"
}
]
}
];
*/

// function Item({ title }) {
//   return (
//     <View style={styles.item}>
//       <Text style={styles.title}>{title}</Text>
//     </View>
//   );
// }

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {DATA: [],
    update:false
};
  }

  getCardDetails() {
    return fetch('https://api.myjson.com/bins/j1msy')
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          DATA: responseJson,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  componentDidMount() {
    this.getCardDetails();
  }


actionOnRow(item) {

var itemSelected = this.state.DATA;
var itemSelectedIndex ;


itemSelected.map((data,index) => {
      if(data == item)
      {
        itemSelectedIndex = data;
        itemSelected[index] = itemSelectedIndex;
      }
    })
if(item.Status == 'Blocked' )
{
Alert.alert(
         'Card Details',
         '',
         [    {text: 'UnBlock', onPress: () => itemSelectedIndex.Status == 'Inactive'},
         {text: 'Permanent Block', onPress: () => itemSelectedIndex.Status == 'Blocked'},
                  {text: 'Reissue Card', onPress: () => itemSelectedIndex.Status == 'Blocked'},
                           {text: 'Cancel', onPress: () => console.log('Permanent Block pressed')},]
        )
}
else
{
  Alert.alert(
         'Card Details',
         '',
         [    {text: 'Block card', onPress: () => itemSelectedIndex.Status == 'Blocked'},
         {text: 'Inactive card', onPress: () => itemSelectedIndex.Status == 'Inactive'},
                  {text: 'ReIssue Card', onPress: () => itemSelectedIndex.Status == 'Blocked'},
                           {text: 'Cancel', onPress: () => console.log('Permanent Block pressed')},]
        )

}

// itemSelected.map((data,index) => {
//       if(data == item)
//       {
//         itemSelected[index] = itemSelectedIndex;
//       }
//     })

//      this.setState({
//           DATA: itemSelected,
//         });


}

  render() {
    return (
      <SafeAreaView style={styles.container}>

      
      <Text style={styles.titleHead}>Debit Cards</Text>

        <FlatList
          data={this.state.DATA}
          renderItem={({item}) => (
         <TouchableWithoutFeedback onPress={ () => this.actionOnRow(item)}>

            <View style={
                item.Status == 'Blocked' ? styles.itemDisable :
                styles.item2}>
               
              <View style={
                styles.item5}>
                <View style={styles.item1} />
               { item.Virtual == true ? <Text style={styles.cardStatus}> Virtual</Text>:
                null}
                { item.Status == 'Blocked' ? <Text style={styles.cardStatus}> Blocked</Text>:
                null}
              </View>

              

              <View style={styles.item}>
                <View style={styles.item3}>
                  <Text style={styles.cardName}>{item.Provider} Debit</Text>

                  <Text style={styles.title}>{item.Id}</Text>
                  
                </View>

                <View style={styles.item4}>
        
<Text style={styles.accountDetails}>{' '}
                    <Text style={{ fontWeight: 'bold',fontSize: 20, }}>₹{item.Accounts[0].Balance}</Text>.00</Text>


                  <Text style={styles.accountDetails}>
                    A/C:{item.Accounts[0].Number}
                  </Text>
                  
                </View>
               
              </View>
                <TouchableOpacity onPress={this._onPressButton}>
      <Image
        style={styles.buttonStyle}
        source={require('./setting.png')}
      />
    </TouchableOpacity>
            </View>
          </TouchableWithoutFeedback>

          )}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
  },
  item5: {
    flexDirection: 'column',
    alignSelf: 'flex-start',

  },
  itemDisable: {
     flexDirection: 'row',
    backgroundColor: '#fafafa',
    alignItems: 'center',
    margin: 20,
    backgroundColor:'rgba(255, 255, 255, 0.2)'
  },
  item4: {
    flexDirection: 'column',
    alignSelf: 'flex-end',
    marginBottom:-20,
    marginTop:20
  },
  item3: {
    flexDirection: 'column',
  },
  item2: {
    flexDirection: 'row',
    backgroundColor: '#fafafa',
    alignItems: 'center',
    margin: 20,
  },
   disableItem: {
    flexDirection: 'row',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    alignItems: 'center',
    margin: 20,
  },
  item1: {
    backgroundColor: '#9c201c',
    width: 100,
    height: 50,
    marginLeft: 10,
    marginTop: 20,
    alignSelf: 'flex-start',
  },
  item: {
    margin: 20,
    width: Dimensions.get('window').width - 200,
  },
  title: {
    fontSize: 14,
  },
  accountDetails: {
    fontSize: 14,
    alignSelf:'flex-end',
  },
  buttonStyle: {
    alignSelf:'flex-start',
    height:30,
    width:30,
    marginLeft:-20,
    marginTop:-40
  },
  cardStatus: {
    fontSize: 16,
    alignSelf:'center',
    marginTop:6,
    fontWeight: 'bold',

    
  },
  cardName: {
    fontSize: 16,
    fontWeight: 'bold',

  },
  titleHead: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft:20,
    color:'white'

  },
});
